<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap"
          rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(52104768, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52104768" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
<div id="app">
    <div class="container-xl">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <a class="navbar-brand font-weight-bolder " href="{{ url('/blog/posts') }}">
                <big>{{ config('app.name', 'Laravel') }}</big>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('blog.posts.index') }}">блог <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#subscribeModal">подписка</a>
                    </li>
                </ul>
            </div>
        </nav>
        <hr class="mt-2 mb-3"/>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="subscribeModal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="subscribeModal">Подпишись и будет тебе счастье. Наверное.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="form" action="{{ action('SubscribesController@subscribe') }}">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">@</span>
                                </div>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                       required>
                            </div>
                            <button type="submit" class="btn btn-primary btn mt-2">Подписаться</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Main -->
    <main role="main" class="container">
        @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Что-то пошло не так!</strong> Извини, но сегодня подписки видимо не будет.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Спасибо, дружище!</strong> Теперь тебе на почту будут приходить оповещения о публикации текстов.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @yield('content')
    </main>

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <hr class="mt-0 mb-1"/>
            <div class="media">
                <div class="media-body">
                    <span class="font-weight-bolder">//Bushe:</span> копируйте что хотите, но не забывайте упоминать
                    меня.
                </div>
                <a href="{{ env('INSTAGRAM_LINK', '#') }}">
                    <img src="{{ asset('images/instagram.svg') }}" width="30" height="30" class="ml-3">
                </a>
                <a href="{{ env('VK_LINK', '#') }}">
                    <img src="{{ asset('images/vk.svg') }}" width="30" height="30" class="ml-3">
                </a>
            </div>
        </div>
    </footer>
</div>
</body>
</html>
