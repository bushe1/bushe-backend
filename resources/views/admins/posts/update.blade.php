@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Админка</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ Illuminate\Support\Str::limit($post->title, 100) }}</li>
        </ol>
    </nav>

    <form action="{{ action('PostsController@destroy', $post) }}" method="post" class="mt-1 mb-3">
        @method('DELETE')
        @csrf
        <button type="submit" class="btn btn-danger">Удалить запись</button>
    </form>

    <form action="{{ action('PostsController@update', $post) }}" method="post">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for="title">Название статьи</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
        </div>
        <div class="form-group">
            <label for="text">Текст</label>
            <textarea class="form-control" name="text" id="text" rows="30">{{ $post->text }}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Обновить</button>
    </form>
@endsection