@extends('layouts.app')

@section('content')
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
               aria-controls="pills-home" aria-selected="true">Редактирование постов</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
               aria-controls="pills-profile" aria-selected="false">Создать новый пост</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="list-group">
                @foreach($posts as $post)
                    <a href="{{ route('admin.form.update', $post) }}"
                       class="list-group-item list-group-item-action">
                        {{ $post->title }}
                        <span class="badge badge-dark">{{ $post->updated_at->format('d.m.Y H:i') }}</span>
                    </a>
                @endforeach
                {{ $posts->links() }}
            </div>
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <form action="{{ action('PostsController@store') }}" method="post">
                @method('POST')
                @csrf
                <div class="form-group">
                    <label for="title">Название статьи</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group">
                    <label for="text" name="text">Текст</label>
                    <textarea class="form-control" name="text" id="text" rows="30"></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Создать</button>
            </form>
        </div>
    </div>
@endsection
