@extends('layouts.app')

@section('content')
    @foreach($posts as $post)
        <div class="blog-post pl-3">
            <h2 class="blog-post-title"><a href="{{ route('blog.posts.show', $post) }}">{{ $post->title }}</a></h2>
            <p class="blog-post-meta">
                <span class="badge badge-dark">{{ $post->updated_at->format('d.m.Y H:i') }}</span>
                <span class="badge badge-warning">{{ $post->chars_count }} символов</span>
            </p>

            <p>{{ Illuminate\Support\Str::limit($post->text, 1000) }}</p>

            <a href="{{ action('PostsController@show', $post) }}" type="button" class="btn btn-secondary btn-sm text-light">Читать далее</a>
            <hr class="mt-2 mb-3"/>
        </div>
    @endforeach
    {{ $posts->links() }}
@endsection