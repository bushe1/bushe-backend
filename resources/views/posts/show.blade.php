@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="{{ url('/blog/posts') }}">Блог</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ Illuminate\Support\Str::limit($post->title, 100) }}</li>
        </ol>
    </nav>

        <div class="blog-post pl-3">
            <h2 class="blog-post-title">{{ $post->title }}</h2>
            <p class="blog-post-meta">
                <span class="badge badge-primary">{{ $post->updated_at->format('d.m.Y H:i') }}</span>
                <span class="badge badge-warning">{{ $post->chars_count }} символов</span>
            </p>

            <p>{{ $post->text }}</p>
        </div>
@endsection