<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Post;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class PostController.
 */
class PostsController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $posts = Post::paginate(5);

        return view('posts.index', ['posts' => $posts]);
    }

    /**
     * @param Post $post
     *
     * @return View
     */
    public function show(Post $post): View
    {
        return view('posts.show', ['post' => $post]);
    }

    /**
     * @param StoreRequest $request
     *
     * @return View
     */
    public function store(StoreRequest $request): View
    {
        $post = $this->createPost($request);

        return view('admins.posts.update', ['post' => $post]);
    }

    /**
     * @param StoreRequest $request
     *
     * @return Post
     */
    private function createPost(StoreRequest $request): Post
    {
        $post = new Post();

        $post->title       = $request->title;
        $post->text        = $request->text;
        $post->chars_count = strlen($request->text);

        $post->save();

        return $post;
    }

    /**
     * @param Post $post
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    public function destroy(Post $post): RedirectResponse
    {
        $post->delete();

        return redirect()->route('admins.home');
    }

    /**
     * @param Post          $post
     * @param UpdateRequest $request
     *
     * @return View
     */
    public function update(Post $post, UpdateRequest $request): View
    {
        $post = $this->updatePost($post, $request);

        return view('admins.posts.update', ['post' => $post]);
    }

    /**
     * @param Post          $post
     * @param UpdateRequest $request
     *
     * @return Post
     */
    private function updatePost(Post $post, UpdateRequest $request): Post
    {
        if (isset($request->title)) {
            $post->title = $request->title;
        }

        if (isset($request->text)) {
            $post->text        = $request->text;
            $post->chars_count = strlen($request->text);
        }

        $post->save();

        return $post;
    }
}
