<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Subscribe\SubscribeRequest;
use Exception;
use Illuminate\Http\RedirectResponse;
use Newsletter;

/**
 * Class SubscribesController.
 */
class SubscribesController extends Controller
{
    /**
     * @param SubscribeRequest $request
     *
     * @return RedirectResponse
     */
    public function subscribe(SubscribeRequest $request): RedirectResponse
    {
        try {
            Newsletter::subscribe($request->email);
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'errors!');
        }

        return redirect()->back()->with('success', 'subscribe!');
    }
}
