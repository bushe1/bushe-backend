<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Post;

class AdminsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::paginate(10);

        return view('admins.home', ['posts' => $posts]);
    }

    /**
     * @param \App\Models\Post $post
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Post $post)
    {
        return view('admins.posts.update', ['post' => $post]);
    }
}
