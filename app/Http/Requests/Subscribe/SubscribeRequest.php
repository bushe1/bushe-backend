<?php

declare(strict_types=1);

namespace App\Http\Requests\Subscribe;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SubscribeRequest.
 *
 * @property string $email
 */
class SubscribeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'email|required',
        ];
    }
}
