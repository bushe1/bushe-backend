<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest.
 *
 * @property string|null $title
 * @property string|null $text
 */
class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'string|max:255',
            'text'  => 'string',
        ];
    }
}
