<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest.
 *
 * @property string $title
 * @property string $text
 * @property int    $user_id
 */
class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'string|required|max:255',
            'text'  => 'string|required',
        ];
    }
}
