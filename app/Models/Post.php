<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Post.
 *
 * @property int                   $id
 * @property string                $title
 * @property string|null           $text
 * @property int|null              $user_id
 * @property Carbon|null           $created_at
 * @property Carbon|null           $updated_at
 * @property \App\Models\User|null $user
 *
 * @method static Builder|Post newModelQuery()
 * @method static Builder|Post newQuery()
 * @method static Builder|Post query()
 * @method static Builder|Post whereCreatedAt($value)
 * @method static Builder|Post whereId($value)
 * @method static Builder|Post whereText($value)
 * @method static Builder|Post whereTitle($value)
 * @method static Builder|Post whereUpdatedAt($value)
 * @method static Builder|Post whereUserId($value)
 * @mixin Eloquent
 *
 * @property int|null $chars_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCharsCount($value)
 */
class Post extends Model
{
    protected $table = 'posts';

    public function user(): belongsTo
    {
        return $this->belongsTo(User::class);
    }
}
