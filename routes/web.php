<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', function () {
    return redirect('/blog/posts');
});

Route::group([
    'prefix' => 'blog',
    'as'     => 'blog.',
], function (): void {
    Route::get('/', function () {
        return redirect('/blog/posts');
    });

    Route::get(
        'subscribe',
        'SubscribesController@subscribe'
    )->name('subscribe');

    Route::resource(
        'posts',
        'PostsController'
    )->only(['index', 'show']);
});

Route::group([
    'prefix'     => 'admin',
    'as'         => 'admin.',
    'middleware' => 'auth',
], function (): void {
    Route::get('', 'AdminsController@index');

    Route::get('posts/{post}', 'AdminsController@update')->name('form.update');

    Route::resource(
        'posts',
        'PostsController'
    )->only(['update', 'destroy', 'store']);
});
